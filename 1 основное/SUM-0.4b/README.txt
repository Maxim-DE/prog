Put the folder "SUM Calculator <version>" wherever you want.
(system folders like "Program Files" are not desirable)

Open the folder and run "sumcalc.exe" file.
That's it, no installation needed!

For convenience, you may also create a shortcut for Desktop or Start menu.

* In case of update - copy these files from previous version folder:
- settings.ini, history.txt, constvars.txt (constants and variables)

----------------------------------------------------------------------

Переместите папку "SUM Calculator <версия>" в любое место,
(системные папки вроде "Program Files" не желательны)

Откройте папку и запустите файл "sumcalc.exe",
Всё, ничего устанавливать не надо!

Для удобства можете создать ярлык на Рабочем Столе или в Меню Пуск.

* В случае обновления - скопируйте следующие файлы с папки предыдущей версии:
- settings.ini (настройки), history.txt (история), constvars.txt (константы и переменные)